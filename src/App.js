import React, { useCallback, useEffect, useState, useRef } from "react";
import ApiService from "./components/Api-service";
import ListEpisodies from "./Lists/ListEpisodies";
import "./App.css";
import { Row, Col,Table, Container, Navbar, Button, Input, FormGroup, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import {MdHighlightOff} from "react-icons/md";


export const AppRickAndMorty = () => {
  const [episodes, setEpisodes] = useState([]);
  const originalEpisodes = useRef(null);
  const [modalFavorites, setModalFavorites] = useState(false);
  const name = useRef(null);
  const [favoritesEpisodes, setFavoritesEpisodes] = useState([]);

  useEffect(() => {
    const loadAll = async () => {
      let list = await ApiService.getEpisodes();
      setEpisodes(list[0].items.results);
      originalEpisodes.current = (list[0].items.results);
    };
    loadAll();
  }, []);

  const onFilter = useCallback((value) => {
    let _filter = originalEpisodes.current;
    if (value === "") {
      setEpisodes(_filter);
      return;
    }

    var filterednames = _filter.filter((obj) => {
      return obj.name
        .toString()
        .toUpperCase()
        .includes(value.toUpperCase());
    });

    setEpisodes(filterednames);
  },[episodes, setEpisodes]);

  const excludeFavorite = useCallback((value) => {
    localStorage.removeItem('list');
    localStorage.clear();
    setFavoritesEpisodes([]);
    setModalFavorites(false);
  },[]);
  
  const loadFavorites = useCallback(() => {
  let _list = [];
    setModalFavorites(true);
    var _arr =  JSON.parse(localStorage.getItem('item'));
    if (_arr !== null) {
      _list.push({
        id: _arr.id,
        name:_arr.name,
        episode: _arr.episode,
        date: _arr.air_date,
        qtd: _arr.characters.length
        });
      setFavoritesEpisodes(_list ? _list : []);
    }
     
  },[setFavoritesEpisodes, favoritesEpisodes]);

  return (
    <div className="divMaster">
        <Modal className="modalEpisodieForId" isOpen={modalFavorites}>
    <ModalHeader
      className="modalHeader "
      close={<Button color="danger" className="close" onClick={function noRefCheck(){setModalFavorites(false)}}>×</Button>}
      toggle={function noRefCheck(){}}
    >
      Episódios favoritos
    </ModalHeader>
    <ModalBody className="divBest">
      <Container>
      <Table responsive striped hover>
                <thead>
                  <tr>
                    <th className="centralized">Id</th>
                    <th className="centralized">Nome</th>
                    <th className="centralized">Episódio</th>
                    <th className="centralized">Data de exibição</th>
                    <th className="centralized">Quantidade de personagens</th>
                    <th className="centralized">Desfavoritar</th>
                  </tr>
                </thead>
                <tbody>
                  {favoritesEpisodes.map((item, index) => (
                    <tr key={index}>
                      {console.log("item", item)}
                      <td className="centralized">{item.id}</td>
                      <td className="centralized">
                        {item.name}
                      </td>
                      <td className="centralized">
                        {item.episode}
                      </td>
                      <td className="centralized">
                        {item.date}
                      </td>
                      <td className="centralized">
                       {item.qtd}
                      </td>
                      <td className="centralized">
                        <Button color="danger" onClick={(e)=> excludeFavorite(e)}><MdHighlightOff style={{fontSize:`30px`}}></MdHighlightOff> </Button>
                      </td>
                      
                    </tr>
                  ))}
                </tbody>
              </Table>
      </Container>
    </ModalBody>
    <ModalFooter>
      <Button color="danger" onClick={function noRefCheck(){setModalFavorites(false)}}>
        Fechar
      </Button>
    </ModalFooter>
  </Modal>
      <FormGroup>
      <div className="divBottom">
        <Row>
          <Col md="3" lg="3" xs="12"> 
          <Button color="danger" onClick={(e) => loadFavorites(e)}>
            Favoritos
          </Button>
          </Col>
          <Col md="6" lg="6" xs="12">
          <Input type="search" placeholder="DIGITE O NOME DO EPISÓDIO" id="search" value={name.current} onChange={(e) => { onFilter(e.target.value)}}>
          </Input>
          </Col>
        </Row>
      </div>
      </FormGroup>

      <section className="lists">
        {episodes.map((item, key) => (
            <ListEpisodies key={key} dataList={item}></ListEpisodies>
        ))}
      </section>
    </div>
  );
};
export default AppRickAndMorty;
