import React, { useCallback, useRef, useState } from "react";
import "./index/index.css";
import { BsFillPersonFill } from "react-icons/bs";
import ApiService from "../components/Api-service";
import {MdDone} from "react-icons/md";
import {MdHighlightOff} from "react-icons/md";
import { Row, Col, Container, Navbar, Button, Input, FormGroup, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
export const ListEpisodies = (props) => {
  
  const [modalEpisodie, setModalEpisodie] = useState(false);
  const [dataEpisode, setDataEpisode] = useState([]);
  const favorites = useRef([]);
  const [eyeVerify, setVerify] = useState(false);

  const takeEpisopieForid = useCallback(async(id) => {
    console.log("item", id);
    setModalEpisodie(true);
    const response = await ApiService.getEpisodieById(id);
    console.log("response", response);
    setDataEpisode(response[0].items);
  },[setModalEpisodie, modalEpisodie]);

  const toggleModalEpisodie = useCallback(() => {
    setModalEpisodie(!modalEpisodie);

  },[]);

  const favoriteEpisodes = useCallback((item) => { 
    localStorage.setItem('item', JSON.stringify(item));
  },[]);

  const eyeTrue = useCallback((item, id, value) => { 
    localStorage.setItem(id, value);
    setVerify(value);
  },[setVerify]);

  const eyeTrueFalse = useCallback((item, id, value) => { 
    localStorage.setItem(id, value);
    setVerify(value);
    localStorage.removeItem(id);
  },[setVerify]);

  return (
    <div className="movie-row" onClick={(e) => takeEpisopieForid(props.dataList.id)}>
       <div>
  <Modal className="modalEpisodieForId" isOpen={modalEpisodie}>
    <ModalHeader
      className="modalHeader "
      close={<Button color="danger" className="close" onClick={function noRefCheck(){setModalEpisodie(false)}}>×</Button>}
      toggle={function noRefCheck(){}}
    >
      Modal title
    </ModalHeader>
    <ModalBody className="divBest">
      <Container>
      <Row>    
        <Col md="3" lg="3" xs="12">
        <div>
        <h3 className="idTitle">Nome </h3>
        <h5 className="nameTitle"> {dataEpisode.name} </h5>
        </div>
        </Col>
        <Col md="3" lg="3" xs="12">
        <div>
        <h3 className="idTitle">Episódio </h3>
        <h5 className="nameTitle"> {dataEpisode.episode} </h5>
        </div>
        </Col>
        <Col md="3" lg="3" xs="12">
        <div>
        <h3 className="idTitle">Data de exibição </h3>
        <h5 className="nameTitle"> {dataEpisode.air_date} </h5>
        </div>
        </Col>
        {eyeVerify ? (
           <Col md="3" lg="3" xs="12">
           <div>
           <h3 className="idTitle">Visto </h3>
           <h5 className="nameTitle"> <Button color="success" onClick={(e)=> eyeTrue(e, dataEpisode.id, false)}><MdDone style={{fontSize:`30px`}}></MdDone> </Button> </h5>
           </div>
           </Col>
        ):(
          <Col md="3" lg="3" xs="12">
          <div>
          <h3 className="idTitle">Visto </h3>
          <h5 className="nameTitle"> <Button color="danger" onClick={(e)=> eyeTrueFalse(e, dataEpisode.id, true)}><MdHighlightOff style={{fontSize:`30px`}}></MdHighlightOff> </Button> </h5>
          </div>
          </Col>
          
        )}
       
      </Row>
      </Container>
    </ModalBody>
    <ModalFooter>
    <Button color="success" onClick={(e) => favoriteEpisodes(dataEpisode)}>
        Favoritar
      </Button>
      <Button color="danger" onClick={function noRefCheck(){setModalEpisodie(false)}}>
        Fechar
      </Button>
    </ModalFooter>
  </Modal>
</div>
      <div className="movieList">
        <h4> {props.dataList.name} </h4>{" "}
        <strong className="person">
          <BsFillPersonFill size={15} />
          {props.dataList.characters.length}{" "}
        </strong>
      </div>
      <div className="divImageRick">
        <img className="imageRick" src={`rick.jpg`}></img>
      </div>
      <p className="valuesNumbers">
        {props.dataList.id} - {props.dataList.episode}
      </p>
    </div>
  );
};
export default ListEpisodies;
