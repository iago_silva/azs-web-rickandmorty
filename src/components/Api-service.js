const api_key = "https://rickandmortyapi.com/api/";

/*
 retornar lista de todos os episódios
 retornar epiósdio pelo id

*/

const fecthAll = async (endpoint) => {
  const requisition = await fetch(`${api_key}${endpoint}`);
  const json = await requisition.json();
  return json;
};

const getFetchById = async (endpoint, id) => {
  const requisition = await fetch(`${api_key}${endpoint}${id}`);
  const json = await requisition.json();
  return json;
};

export default {
  getEpisodes: async () => {
    return [
      {
        slug: "Episódios",
        title: "Rick and morty",
        items: await fecthAll("/episode"),
      },
    ];
  },
  getEpisodieById: async (id) => {
    return [
      {
        slug: "Episódio pelo Id",
        title: "Rick and morty",
        items: await getFetchById("episode/", id),
      },
    ];
  },
};
